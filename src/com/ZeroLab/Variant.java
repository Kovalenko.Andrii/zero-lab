package com.ZeroLab;

import java.util.Objects;

public class Variant {

    public static class Date{
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Date date = (Date) o;
            return day == date.day &&
                    month == date.month;
        }

        @Override
        public int hashCode() {
            return Objects.hash(day, month);
        }

        private  int day;
        private int month;

        public Date() {
        }



        public Date(int day, int month) {
            this.day = day;
            this.month = month;
        }
    }
    public  static  class Swap{
        private double A;
        private double B;

        public Swap(double a, double b) {
            A = a;
            B = b;
        }

        public Swap() {
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Swap swap = (Swap) o;
            return A == swap.A &&
                    B == swap.B;
        }

        @Override
        public int hashCode() {
            return Objects.hash(A, B);
        }
    }

    public  double Begin9(double A, double B)
    {
        return Math.sqrt(A*B);
    }
    public  int Integer9(int A)
    {
        return A/100;
    }
    public  boolean Boolean9(int A,int B)
    {
        if((A%2==0)|(B%2==0)) return true;
        else return false;
    }
    public  Swap if9(double A,double B) {
        double temp = 0.0;
        Swap result = new Swap(A, B);

        if (result.A > result.B) {
            temp = result.A;
            result.A = result.B;
            result.B = temp;
        }
        System.out.println(result.A);
        System.out.println(result.B);
        return result;
    }
    public  Date switch9(int D, int M)
    {
        Date rezult = new Date();
        int NextD, NextM;
    switch (M) {
        case 1: //31 d in m
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
            if (D < 31) {
                D++;
                //System.out.println("Enter to switch");
            }
            if (D == 31) {
                D = 1;
                M++;
               // System.out.println("Enter to switch");
            }
            break;
        case 12:
            if (D < 31) {
                D++;
            }
            if (D == 31) {
                D = 1;
                M = 1;
            }
            break;
        case 4: //30 d in m
        case 6:
        case 9:
        case 11:
            if (D < 30) {
                D++;
            }
            if (D == 30)
            {
                D = 1;
                M++;
            }
            break;
        case 2:
            if (D < 28) {
                D++;
            }
            if (D == 28)
            {
                D = 1;
                M++;
            }
            break;
        default:
            System.out.println("No correct M");
            break;
    }
    rezult.day=D;
    rezult.month=M;
    return rezult;
    }
    public  int for9(int A, int B) {
        int sum = 0;
        for (int i = A; i <= B; i++) {
            sum += i * i;
        }
        return sum;
    }
    public  int while9 (int N){
        int k = 0;
        while (3*k<=N)
        {
            k++;
        }
        return k-1;
    }
    public int array9 (int N){
        int[] Array = new int[N];
        for (int i = 0; i < Array.length ; i++){
            //Array[i] = (int) Math.round((Math.random() * 30) - 15);
            Array[i] = i+1;
        }

        for(int i = 0; i<Array.length-1; i++)
        {
            if( ( (Array[i]%2==0) && (Array[i+1]%2!=0) ) | ( (Array[i]%2!=0) && (Array[i+1]%2==0) )  ) continue;
            else return i+1;
        }
        return 0;
    }
    public int[][] matrix9(int N, int M){
       int[][] Matrix = new int[M][N]; //M%2==0
        for(int i = 0 ; i < M; i++) {
            for(int j = 0; j < N; j++){
              Matrix[i][j] = i+j;
            }

        }
        for(int i = 0 ; i < M; i++) {
            for(int j = 0; j < N; j++){
                System.out.print(Matrix[i][j] + "\t");
            }
        System.out.println();
        }
        int t ;
        for (int i = 0; i < M/2; i++)
            for (int j = 0; j < N; j++){
                t = Matrix[i][j];
                Matrix[i][j] = Matrix[M / 2 + i][j];
                Matrix[M / 2 + i][j] = t;

            }

        for(int i = 0 ; i < M; i++) {
            for(int j = 0; j < N; j++){
                System.out.print(Matrix[i][j] + "\t");
            }
            System.out.println();
        }
        return Matrix;
    }

}
