package TestPackage;

import static org.testng.Assert.assertEquals;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ZeroLab.Variant;
public class TestVariant {
    private  Variant variant9 = new Variant();

    @Test
    public void TestBegin9() throws Exception{
        Assert.assertEquals(2,variant9.Begin9(2,2));
    }
    @Test
    public void TestInteger9() throws Exception{
        Assert.assertEquals(3, variant9.Integer9(324));
    }
    @Test
    public  void TestBoolean9() throws  Exception{
        Assert.assertEquals(true,variant9.Boolean9(5,6));
    }
    @Test
    public  void TestFor9() throws  Exception{
        Assert.assertEquals(30,variant9.for9(1,4));
    }
    @Test
    public  void TestWhile9() throws  Exception{
        Assert.assertEquals(3,variant9.while9(9));
    }
    @Test
    public  void TestArrray9() throws  Exception{
        Assert.assertEquals(0,variant9.array9(6));
    }
    @Test
    public void testSwitch() {
        Assert.assertEquals( new Variant.Date(1,1) , variant9.switch9 (31,12)  );
    }
    @Test
    public void testIf() {
        Assert.assertEquals( new Variant.Swap(12.7,13.5) , variant9.if9(13.5,12.7)  );
    }
    @Test
    public void testMatrix() {
       int[][] matr = {{2,3,4,5,6},{3,4,5,6,7,},{0,1,2,3,4},{1,2,3,4,5}} ;
        Assert.assertEquals( matr , variant9.matrix9(5,4)  );
    }
}
